// alert(`Hello`)

const getCube = 9 ** 3
// console.log(getCube)

let message = `The cube of 9 is ${getCube}.`
console.log(message)

const address = [304, `Sta. Rita`, `Guiguinto`, `Bulacan`]

const [houseNumber, brgy, municipality, province] = address

console.log(`I live at ${houseNumber} ${brgy} ${municipality}, ${province}`)

const animal = {
	name: `Kerby`,
	breed: `American Bully`,
	weigth: 25
}

const {name, breed, weigth} = animal

console.log(`${name} was a ${breed} dog. He weigthed at ${weigth} kgs.`)

const numbers = [10, 20, 30, 40]

numbers.forEach(number => console.log(number))

class Dog {
	constructor(name,age,breed) {
		this.name = name,
		this.age = age,
		this.breed = breed
	}
}
const myDog = new Dog(`Kelly`, 3, `shih tzu`)
console.log(myDog)